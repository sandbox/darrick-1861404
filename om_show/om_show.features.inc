<?php
/**
 * @file
 * om_show.features.inc
 */

/**
 * Implements hook_views_api().
 */
function om_show_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function om_show_node_info() {
  $items = array(
    'om_show' => array(
      'name' => t('Episode'),
      'base' => 'node_content',
      'description' => t('<b>Open Media System</b> - This content type where the shows metadata is stored.  The actual videos for a show are managed by the Media Mover module.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
