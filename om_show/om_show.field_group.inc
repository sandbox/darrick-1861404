<?php
/**
 * @file
 * om_show.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function om_show_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_metadata|node|om_show|form';
  $field_group->group_name = 'group_metadata';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'om_show';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'metadata',
    'weight' => '4',
    'children' => array(
      0 => 'taxonomy_vocabulary_21',
      1 => 'taxonomy_vocabulary_19',
      2 => 'taxonomy_vocabulary_20',
      3 => 'field_om_locally_produced',
      4 => 'field_cc',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_metadata|node|om_show|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_old_data|node|om_show|form';
  $field_group->group_name = 'group_old_data';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'om_show';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Old',
    'weight' => '7',
    'children' => array(
      0 => 'taxonomy_vocabulary_22',
      1 => 'taxonomy_vocabulary_3',
      2 => 'taxonomy_vocabulary_1',
      3 => 'taxonomy_vocabulary_2',
      4 => 'taxonomy_vocabulary_4',
      5 => 'taxonomyextra',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_old_data|node|om_show|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_details|node|om_show|default';
  $field_group->group_name = 'group_show_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'om_show';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '2',
    'children' => array(
      0 => 'taxonomy_vocabulary_21',
      1 => 'taxonomy_vocabulary_19',
      2 => 'taxonomy_vocabulary_20',
      3 => 'field_om_show_duration',
      4 => 'field_om_show_tightrope_id',
      5 => 'field_om_show_episode_number',
      6 => 'group_audience',
      7 => 'field_cc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_show_details|node|om_show|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_details|node|om_show|teaser';
  $field_group->group_name = 'group_show_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'om_show';
  $field_group->mode = 'teaser';
  $field_group->parent_name = 'group_show_div_left';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '5',
    'children' => array(
      0 => 'taxonomy_vocabulary_21',
      1 => 'taxonomy_vocabulary_19',
      2 => 'taxonomy_vocabulary_20',
      3 => 'field_om_show_duration',
      4 => 'field_om_show_tightrope_id',
      5 => 'field_om_show_episode_number',
      6 => 'group_audience',
      7 => 'field_cc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Details',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_show_details|node|om_show|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_div_left|node|om_show|teaser';
  $field_group->group_name = 'group_show_div_left';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'om_show';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'group_show_details',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_show_div_left|node|om_show|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_video|node|om_show|form';
  $field_group->group_name = 'group_video';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'om_show';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Video',
    'weight' => '3',
    'children' => array(
      0 => 'field_mediafront',
      1 => 'field_om_show_duration',
      2 => 'field_screenshot',
      3 => 'field_video',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_video|node|om_show|form'] = $field_group;

  return $export;
}
