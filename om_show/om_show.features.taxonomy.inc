<?php
/**
 * @file
 * om_show.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function om_show_taxonomy_default_vocabularies() {
  return array(
    'vocabulary_1' => array(
      'name' => 'Cablecast Format',
      'machine_name' => 'vocabulary_1',
      'description' => 'For managing offline media.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'vocabulary_19' => array(
      'name' => 'PBCore Genres',
      'machine_name' => 'vocabulary_19',
      'description' => '',
      'hierarchy' => '1',
      'module' => 'pbcore',
      'weight' => '-10',
    ),
    'vocabulary_2' => array(
      'name' => 'Tags',
      'machine_name' => 'vocabulary_2',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'vocabulary_20' => array(
      'name' => 'PBCore Languages',
      'machine_name' => 'vocabulary_20',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'pbcore',
      'weight' => '-10',
    ),
    'vocabulary_21' => array(
      'name' => 'PBCore FCC Ratings',
      'machine_name' => 'vocabulary_21',
      'description' => '',
      'hierarchy' => '1',
      'module' => 'pbcore',
      'weight' => '-10',
    ),
    'vocabulary_22' => array(
      'name' => 'PBCore MPAA Ratings',
      'machine_name' => 'vocabulary_22',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'pbcore',
      'weight' => '-10',
    ),
    'vocabulary_23' => array(
      'name' => 'Accessories',
      'machine_name' => 'vocabulary_23',
      'description' => '',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'vocabulary_3' => array(
      'name' => 'Add to Library_Return_or_Recycle',
      'machine_name' => 'vocabulary_3',
      'description' => '',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'vocabulary_4' => array(
      'name' => 'Time Dated or Don\'t Repeat',
      'machine_name' => 'vocabulary_4',
      'description' => '',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'vocabulary_5' => array(
      'name' => 'Website',
      'machine_name' => 'vocabulary_5',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'vocabulary_7' => array(
      'name' => 'Channel',
      'machine_name' => 'vocabulary_7',
      'description' => '',
      'hierarchy' => '1',
      'module' => 'om_timeslot_scheduler',
      'weight' => '-10',
    ),
    'vocabulary_8' => array(
      'name' => 'MERCI Equipment Grouping',
      'machine_name' => 'vocabulary_8',
      'description' => NULL,
      'hierarchy' => '0',
      'module' => 'merci',
      'weight' => '-10',
    ),
  );
}
